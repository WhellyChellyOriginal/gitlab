---
stage: Solutions Architecture
group: Solutions Architecture
info: This page is owned by the Solutions Architecture team.
---

# AWS Solutions

This documentation covers solutions relating to Amazon Web Services (AWS).

[GitLab AWS Integration Index](gitlab_aws_integration.md)
